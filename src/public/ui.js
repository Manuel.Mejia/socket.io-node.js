import { saveNote, deleteNote, getNoteUpdate, updateNote } from './socket.js'

const noteList = document.querySelector("#notes");

let saveId = "";

const noteUI = (note) => {
    const div = document.createElement('div');
    div.innerHTML += `
    <div class="card card-body rounded-0 animate__animated animate__fadeInUp mb-2">
    <div class="d-flex justify-content-between">
        <h1 class="card-title h3">${note.title}</h1>
        <div>
            <button class="btn btn-danger delete" data-id="${note._id}">delete</button>
            <button class="btn btn-secondary update" data-id="${note._id}">update</button>
        </div>
    </div>
        <p>${note.description}</p>
    </div>
    `
    const btnDelete = div.querySelector('.delete');
    const btnUpdate = div.querySelector('.update');

    btnDelete.addEventListener('click', (e) => { deleteNote(btnDelete.dataset.id) })
    btnUpdate.addEventListener('click', (e) => { getNoteUpdate(btnUpdate.dataset.id) })
    return div
}

export const renderNotes = (notes) => {
    noteList.innerHTML = "";
    notes.forEach(note => { noteList.append(noteUI(note)) });
}

export const onHandleSubmit = (e) => {
    e.preventDefault();

    if (saveId) {
        updateNote(saveId, noteForm["title"].value, noteForm["description"].value)
    } else {
        saveNote(noteForm["title"].value, noteForm["description"].value);
    }

    noteForm["title"].value = "";
    noteForm["description"].value = "";
}

export const onSaveNote = (note) => {
    noteList.append(noteUI(note));
}

export const fillForm = (note) => {
    noteForm["title"].value = note.title;
    noteForm["description"].value = note.description;
    saveId = note._id
}
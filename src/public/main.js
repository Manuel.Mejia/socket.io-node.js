import {loadNotes, onNewNote, onSelected} from './socket.js';
import {onHandleSubmit, renderNotes, onSaveNote, fillForm} from './ui.js';

onNewNote(onSaveNote);
loadNotes(renderNotes);
onSelected(fillForm);

const noteForm = document.querySelector('#noteForm');
noteForm.addEventListener('submit', onHandleSubmit);

import pkg from 'mongoose'
const {Schema, model} = pkg;

const noteSchema = new Schema({
    title : {
        type: String,
        required : true

    },
    description: {
        type: String
    }
},{
    timestamps: true
})

export default model('Note', noteSchema)
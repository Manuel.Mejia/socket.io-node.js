import app from './app.js';
import {Server as websocketserver} from 'socket.io';
import http from 'http';
import {connectDB} from './db.js';
import sockets from './sockets.js';

connectDB()
const server = http.createServer(app);
const httpServer = server.listen(3000, ()  => {
    console.log('server running on port 3000')
})
const io = new websocketserver(httpServer);
sockets(io);
import Note from './models/Note.js'

export default (io) =>{

    io.on('connection', (socket) => {
        const emitNote = async () => {
            const notes = await Note.find();            
            io.emit('server:loadnotes', notes)
            
        }
        emitNote();

        socket.on("client:savenote", async (data) => {
            const newNote =  new Note(data);
            const saveNote = await newNote.save();
            io.emit('server:savenote', saveNote);
        });

        socket.on('client:deletenote', async (id) => {            
            await Note.findByIdAndDelete(id)
            emitNote()
        });

        socket.on('client:getnote', async (id) => {
            const note = await Note.findById(id);
            socket.emit('server:getnoteupdate', note);
        });

        socket.on('client:updatenote', async (data) => {
            await Note.findByIdAndUpdate(data._id ,{
                title : data.title,
                description : data.description
            })
            emitNote()
        });
    })
}